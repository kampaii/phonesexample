﻿using PhonesExample.exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhonesExample.behaviour
{
    class PhoneBehaviour
    {
        private Phone phone;

        public PhoneBehaviour(Phone phone)
        {
            this.phone = phone;
        }

        // поведение телефона - добавляет контакт второго телефона и вызывает
        public void AddContactAndMakeCall()
        {
            // спим 2 сек чтобы поток 2 точно успел запуститься
            Thread.Sleep(2000);
            Console.WriteLine("thread Phone: " + phone.ToString() + " has started");

            try
            {
                phone.Connect();

                this.phone.AddContact(new Contact("MyFriend", "123321"));
                Console.WriteLine(this.phone.ToString()+ "> добавил контакт");
               
                this.phone.MakeCall(this.phone.Contacts[0]);
            }
            catch(Exception ex)
            {
                if(ex is ConnectionClosedException)
                {
                    Console.WriteLine("Соединение закрыто");
                }
                else { 
                    Console.WriteLine(phone.ToString() + " raised an exception: " + ex.ToString());
                }
            }


            Console.WriteLine("thread Phone: " + phone.ToString() + " has ended");
        }

        // поведение второго телефона - ожидание звонка
        public void WaitForCall()
        {
            Console.WriteLine("thread Phone: " + phone.ToString() + " has started");

            try
            {
                phone.Connect();

                Thread.Sleep(1000);
                
                for (int i = 0; i < 40; i++)
                {
                    Console.WriteLine(phone.ToString() + " PhoneBehaviour.WaitForCall > ожидаем звонка " + i);
                    this.phone.AcceptCall();
                    Thread.Sleep(2000);
                }
            }
            catch (Exception ex)
            {
                if (ex is ConnectionClosedException)
                {
                    Console.WriteLine(phone.ToString()+" - соединение закрыто");
                }
                else
                {
                    Console.WriteLine(phone.ToString() + " raised an exception: " + ex.ToString());
                }
            }

            Console.WriteLine("thread Phone: " + phone.ToString() + " has ended");
        }
    }
}
