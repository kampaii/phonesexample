﻿using PhonesExample.behaviour;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhonesExample
{
    class Program
    {
        static void Main(string[] args)
        {
            // общая сеть
            Net net = new Net();
            // две базы
            Base b1 = new Base(net);
            Base b2 = new Base3G(net);
            //два телефона
            Phone ph1 = new Phone(IMEI : 1, net: net, SIMnumber: "9033541425");
            Phone ph2 = new Phone(IMEI: 2, net: net, SIMnumber: "123321");
            // ph1 будет звонить
            PhoneBehaviour bh = new PhoneBehaviour(ph1);
            Thread th = new Thread(new ThreadStart(bh.AddContactAndMakeCall));
            // ph2 будет принимать звонок
            PhoneBehaviour bh2 = new PhoneBehaviour(ph2);
            Thread th2 = new Thread(new ThreadStart(bh2.WaitForCall));

            th.Start();
            th2.Start();
           
            th.Join();
            th2.Join();

            Console.Read();
        }
    }
}
