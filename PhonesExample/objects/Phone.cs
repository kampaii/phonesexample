﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhonesExample
{
    // телефон, хранит IMEI без возможности редактирования, редактируемый номер SIM и редактируемый справочник контактов
    class Phone
    {
        public readonly int IMEI;

        // ссылка на "сеть" - через нее будем пытаться подключаться к базам
        private Net net;
        // ссылка на текущую базу - к ней обращаемся для соединения
        private Base currentBase;
        // адресная книга
        private List<Contact> contacts;
        //занят ли номер сейчас
        private bool isOnCall;
        // входящее соединение
        private Connection incomingConnection;

        // номер СИМ ставим только изнутри
        private string sIMnumber;

        public string SIMnumber { get => sIMnumber; private set => sIMnumber = value; }
        internal List<Contact> Contacts { get => contacts; private set => contacts = value; }
        public bool IsOnCall { get => isOnCall;private set => isOnCall = value; }
        public Connection IncomingConnection { get => incomingConnection; set => incomingConnection = value; }

        public Phone(int IMEI, Net net, string SIMnumber)
        {
            this.IMEI = IMEI;
            this.net = net;
            this.SIMnumber = SIMnumber;
            IsOnCall = false;
            this.Contacts = new List<Contact>();
        }

        #region работа с телефонной книгой
        public void AddContact(Contact contact)
        {
            if ( !this.Contacts.Contains(contact))
            {
                this.Contacts.Add(contact);
            }
        }

        public void RemoveContact(Contact contact)
        {
            this.Contacts.Remove(contact);
        }
        #endregion

        #region работа со звонками
        // использует звонок на номер
        public virtual void MakeCall(Contact contact)
        {
            this.MakeCall(contact.PhoneNumber);
        }

        public virtual void MakeCall(string phoneNumber)
        {
            if ( this.currentBase is null )
            {
                throw new Exception(this.ToString()+ " > Нельзя звонить без подключения к базе!");
            }

            Console.WriteLine(this.ToString() + " calls " + phoneNumber);

            this.IsOnCall = true;

            // пытаемся соединиться
            Connection conn = null;
            try
            {
                // 50 секунд пытаемся настроить соединение
                conn = this.currentBase.CallRequest(this, phoneNumber,50);
                Console.WriteLine(this.ToString() + " получил соединение с " + phoneNumber);
            }
            catch( Exception ex)
            {
                if ( ex is KeyNotFoundException)
                {
                    // номер не найден            
                    Console.WriteLine(this.ToString() + " не получил соединение с " + phoneNumber + " - номер не найден");
                    return;
                }
                else
                {
                    // иначе пусть и дальше выкидывает
                    throw;
                }
            }

            for (int i = 0; i < Net.rnd.Next(20,40); i++)
            {
                string message = "[" + i + "]";
                Console.WriteLine(this.ToString() + " отправляет : " + message);
                conn.WriteBuffer(true, message);
                string[] buf = conn.ReadBuffer(true);
                foreach (var item in buf)
                {
                    Console.WriteLine(this.ToString()+" получает : "+item);
                }
                Thread.Sleep(1000);
            }

            Console.WriteLine(this.ToString() + " Завершает звонок");

            conn.Disconnect();

            IsOnCall = false;
        }

        // поведение для телефона, принимающего звонок
        public virtual void AcceptCall()
        {
            if( this.IncomingConnection != null)
            {
                if (this.incomingConnection.State == "REQUESTED")
                {
                    this.incomingConnection.ConnectionAccepted();

                    Console.WriteLine(this.ToString() + " > Соединение принято");

                    for (int i = 0; i < Net.rnd.Next(20, 40); i++)
                    {
                        string message = "[" + i + "]";
                        Console.WriteLine(this.ToString() + " отправляет : " + message);
                        this.incomingConnection.WriteBuffer(true, message);
                        string[] buf = this.IncomingConnection.ReadBuffer(false);
                        foreach (var item in buf)
                        {
                            Console.WriteLine(this.ToString() + " получает : " + item);
                        }
                        Thread.Sleep(1000);
                    }
                    this.IncomingConnection.Disconnect();
                }

                if(this.IncomingConnection.State == "CLOSED")
                {
                    this.incomingConnection = null;
                }
            }

            IsOnCall = false;
        }
        #endregion

        #region подключение к базе
        // pulblic вынес только чтобы использовать в PhoneBehaviour
        public void Connect()
        {
            while(this.currentBase is null)
            {
                // находим базу в сети    
                Base base_ =  this.net.DiscoverBase(this);
                // пытаемся подключиться
                if (base_.Connect(this))
                {
                    Console.WriteLine( this.ToString() +  " подключился к базе");
                    this.currentBase = base_;
                }
                else
                {
                    // ждем 20 секунд и пытаемся снова
                    Thread.Sleep(20);
                }
            }

        }

        public void Disconnect()
        {
            if(this.currentBase != null)
            {
                if (this.currentBase.Disconnect(this))
                {
                    Console.WriteLine(this.ToString() + " Отключился от базы");
                }
            }

        }
        #endregion

        override
        public string ToString()
        {
            return "Телефон " + this.IMEI + " SIM " + this.SIMnumber;
        }
    }

}
