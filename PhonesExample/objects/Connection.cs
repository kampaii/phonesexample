﻿using PhonesExample.exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PhonesExample
{
    class Connection
    {
        private string state;
        private Phone requestor;
        private Phone recipient;

        private List<string> bufferToRecipient;
        private List<string> bufferToRequestor;

        public string State { get => state; private set => state = value; }

        public Connection(Phone requestor, Phone recipient, int timeout)
        {
            this.requestor = requestor;
            this.recipient = recipient;

            this.bufferToRecipient = new List<string>();
            this.bufferToRequestor = new List<string>();

            this.state = "CREATED";
            EstablishConnection(timeout);
        }

        // order - true - для requestor, false - для recipient
        public void WriteBuffer(bool order, string message)
        {
            if (!this.state.Equals("WORK"))
            {
                throw new ConnectionClosedException("Соединение прервано");
            }

            if ( order )
            {
                lock (this.bufferToRecipient)
                {
                    this.bufferToRecipient.Add(message);
                }
            }
            else
            {
                lock (this.bufferToRequestor)
                {
                    this.bufferToRequestor.Add(message);
                }
            }
        }

        // чтение буфера order - true - для requestor, false - для recipient
        public string[] ReadBuffer(bool order)
        {
            if (!this.state.Equals("WORK"))
            {
                throw new ConnectionClosedException("Соединение прервано");
            }

            if ( !order )
            {
                lock ( this.bufferToRecipient)
                {
                    var res = new string[this.bufferToRecipient.Count];
                    this.bufferToRecipient.CopyTo(res);
                    this.bufferToRecipient.Clear();
                    return res;
                }
            }
            else
            {
                lock( this.bufferToRequestor)
                {
                    var res = new string[this.bufferToRequestor.Count];
                    this.bufferToRequestor.CopyTo(res);
                    this.bufferToRequestor.Clear();
                    return res;
                }
            }
        }

        // поведение запрашивающего соединение
        public void EstablishConnection(int timeout)
        {
            DateTime t = DateTime.Now.AddSeconds(timeout);
            while(this.state == "CREATED")
            {
                Console.WriteLine(this.requestor.ToString() + " Устанавливаем соединение Connection.EstablishConnection > " + this.state);
                if (DateTime.Now >= t)
                {
                    throw new Exception("Достигнут таймаут соединения");
                }
                if (!this.recipient.IsOnCall)
                {
                    Console.WriteLine("Перевел соединение в состояние REQUESTED");
                    this.recipient.IncomingConnection = this;
                    this.state = "REQUESTED";
                    break;
                }
                Thread.Sleep(1000);
            }

            while(DateTime.Now <= t)
            {
                Console.WriteLine(this.requestor.ToString() + " Жду принятия вызова. Состояние соединения: "+this.state);
                if (this.state == "WORK")
                {
                    return;
                }
                Thread.Sleep(1000);
            }
            throw new ConnectionClosedException("Достигнут таймаут соединения");
        }

        // Принятие соединения recipient-ом
        public void ConnectionAccepted()
        {
            this.state = "WORK";
        }

        // отключение соединения любой из сторон
        public void Disconnect()
        {
            lock (this.state)
            {
                this.state = "CLOSED";
            }
        }
    }
}
