﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonesExample
{  
    // эмуляция радиоэфира, формально через него телефон ищет подходящую станцию
    class Net
    {
        // знает о станциях, о телефонах ей знать не обзательно
        private List<Base> bases;

        public static Random rnd = new Random();

        public Net()
        {
            this.bases = new List<Base>();
        }

        public void addBase(Base base_)
        {
            if (!this.bases.Contains(base_))
            {
                this.bases.Add(base_);
            }
        }

        // выдает случайную базу для подключения, будем считать что так симулируем выбор самой подходящей базы из ближайших
        public Base DiscoverBase(Phone phone)
        {
            // TODO проверить на генерацию исключений
            return this.bases[rnd.Next(this.bases.Count())];
        }

        // поиск базы у которой зарегистрирован данный номер
        public Base FindEndpoint(Base requestor, string callNumber)
        {
            foreach ( Base b in this.bases )
            {
                // если мы сюда пришли, значит уже не нашли телефон в requestor-е
                if ( b == requestor)
                {
                    continue;
                }

                if ( b.IsRegistered(callNumber) )
                {
                    return b;
                }
            }

            return null;
        }
    }
}
