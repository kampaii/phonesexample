﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonesExample
{
    class Base3G : Base
    {
        // 3g станция запоминает 3g телефоны отдельно
        private Dictionary<string, Phone> registeredPhones3g;

        public Base3G(Net net) : base(net)
        {
            this.registeredPhones3g = new Dictionary<string, Phone>();
            base.ConnectToNet();
        }

        public override bool Connect(Phone phone)
        {
            // по особому работаем c 3g
            if( phone is Phone3G)
            {
                if (!registeredPhones3g.ContainsKey(phone.SIMnumber))
                {
                    this.registeredPhones3g.Add(phone.SIMnumber, phone);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return base.Connect(phone);
            }

        }

        public override bool Disconnect(Phone phone)
        {
            // по особому работаем c 3g
            if (phone is Phone3G)
            {
                if (registeredPhones3g.ContainsKey(phone.SIMnumber))
                {
                    this.registeredPhones3g.Remove(phone.SIMnumber);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return base.Disconnect(phone);
            }
        }

        public override bool IsRegistered(string callNumber)
        {
            if(this.registeredPhones.ContainsKey(callNumber) || this.registeredPhones3g.ContainsKey(callNumber))
            {
                return true;
            }
            return false;
        }
    }
}
