﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonesExample
{
    // контакт для хранения в адресной книге телефона
    class Contact
    {
        private string fullName;
        private string phoneNumber;

        public Contact(string fullName, string phoneNumber)
        {
            this.FullName = fullName;
            this.phoneNumber = phoneNumber;
        }

        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string FullName { get => fullName; set => fullName = value; }
    }
}
