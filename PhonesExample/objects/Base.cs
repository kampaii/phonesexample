﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonesExample
{
    // базовая станция
    class Base
    {
        // Станция знает инфу о телефонах, которые к ней подключены
        public Dictionary<string,Phone> registeredPhones;

        // станция знает свою сеть
        private Net net;

        public Base(Net net)
        {
            this.net = net;
            this.registeredPhones = new Dictionary<string,Phone>();
            ConnectToNet();
        }

        #region подключение/отключение
        protected void ConnectToNet()
        {
            this.net.addBase(this);
        }

        public virtual bool Connect(Phone phone)
        {
            if( !this.registeredPhones.ContainsKey(phone.SIMnumber))
            {
                this.registeredPhones.Add(phone.SIMnumber, phone);
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual bool Disconnect(Phone phone)
        {
            this.registeredPhones.Remove(phone.SIMnumber);
            return true;
        }
        #endregion

        //запрос, зарегистрирован ли телефон на данной базе
        public virtual bool IsRegistered(string callNumber)
        {
            if (this.registeredPhones.ContainsKey(callNumber))
            {
                return true;
            }
            return false;
        }

        // запрос на звонок от телефона на номер
        public virtual Connection CallRequest(Phone caller, string callNumber,int timeout)
        {
            Console.WriteLine("База. Получен запрос от " + caller.SIMnumber + " на соединение с " + callNumber);
            // если номер подключен к текущей станции, возвращаем соединение
            if (IsRegistered(callNumber))
            {
                // todo номер может быть занят
                return new Connection(caller, this.registeredPhones[callNumber],timeout);
            }
            // иначе по всем доступным станциям ищем наш телефон
            Base recipientBase = this.net.FindEndpoint(this, callNumber);
            // если не нашли
            if (recipientBase is null)
            {
                throw new KeyNotFoundException("Не найден абонент "+callNumber);
            }
            // а иначе возвращаем соединение через ту базу
            return recipientBase.CallRequest(caller, callNumber,timeout);
            
        }
    }
}
