﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhonesExample.exceptions
{
    class ConnectionClosedException : Exception
    {
        public ConnectionClosedException(string message) : base(message)
        {

        }
    }
}
